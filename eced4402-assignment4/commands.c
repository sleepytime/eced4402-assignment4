/*
 * commands.c
 *
 *  Created on: 2015-11-27
 *      Author: Andy
 */

/*
 * Atmel 1 - Hall sensor signals (1-8), section control, hall sensor reset (all sensors)
 * Atmel 2 - Switch control (1-9)
 * Atmel 3 - Hall sensor signal (9-32)
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "packet.h"
#include "spi.h"

//void sendPacket(unsigned char packet, int atmel)
//{
//	int i;
//	for(i=0; i<PACKET_SIZE; i++)
//	{
//		SPIDataTransmit(packet[i], atmel);
//		WaitSPIBusy();
////		result[i] = SPIDataReceive();
//	}
////	if(result[i-1] == 0x3F) //Check if an error occurred
////	{
////		// Shutdown everything
////		powerAllOff();
////		exit(1); // 0 = exit for a success, 1 = exit for a failure
////	}
//}

void powerOn(unsigned int section, unsigned int direction, unsigned int magnitude)
{
	CMDPacket powerOn;
	RECVPacket powerOnRecv;

//	printf("In the powerOn function\n");

	powerOn.start = 0xAA;
	powerOn.cmd = 0xB0;
	powerOn.arg1 = (section & 0xFF); // Makes sure it's 1 byte
	powerOn.arg2 =  (((direction & 0xFF) << 7) | magnitude);
	powerOn.end1 = 0x55;
	powerOn.end2 = 0xFF;

//	sendPacket((unsigned char*)&powerOn, 1, (unsigned char*)&powerOnRecv);
}

void powerOff(unsigned int section)
{
	CMDPacket powerOff;
	RECVPacket powerOffRecv;

	powerOff.start = 0xAA;
	powerOff.cmd = 0xB0;
	powerOff.arg1 = section & 0xFF;
	powerOff.arg2 = 0x00;	// Set magnitude to 0. Direction doesn't matter.
	powerOff.end1 = 0x55;
	powerOff.end2 = 0xFF;

//	sendPacket((unsigned char*)&powerOff, 1, (unsigned char*)&powerOffRecv);
}

void powerAllOn(unsigned int direction, unsigned int magnitude)
{
	CMDPacket powerAllOn;
	RECVPacket powerAllOnRecv;

	unsigned char mag;
	unsigned char dir;

	if(magnitude > 7) // set upper bound
	{
		magnitude = 7;
	}

	mag = magnitude + '0';
	dir = direction + '0';
	powerAllOn.start = 0xAA;
	powerAllOn.cmd = 0xBE;
	powerAllOn.arg1 = 0xFF;
	powerAllOn.arg2 = (dir << 7) | mag;
	powerAllOn.end1 = 0x55;
	powerAllOn.end2 = 0xFF;

	SPIDataTransmit(powerAllOn.start, 1);
	SPIDataTransmit(powerAllOn.cmd, 1);
	SPIDataTransmit(powerAllOn.arg1, 1);
	SPIDataTransmit(powerAllOn.arg2, 1);
	SPIDataTransmit(powerAllOn.end1, 1);
	SPIDataTransmit(powerAllOn.end2, 1);

//	sendPacket((unsigned char*)&powerAllOn, 1, (unsigned char*)&powerAllOnRecv);
}

void powerAllOff()
{
	CMDPacket powerAllOff;
	RECVPacket powerAllOffRecv;

	powerAllOff.start = 0xAA;
	powerAllOff.cmd = 0xBE;
	powerAllOff.arg1 = 0xFF;
	powerAllOff.arg2 = 0x00; // set magnitude to 0. direction doesn't matter.
	powerAllOff.end1 = 0x55;
	powerAllOff.end2 = 0xFF;

	SPIDataTransmit(powerAllOff.start, 1);
	SPIDataTransmit(powerAllOff.cmd, 1);
	SPIDataTransmit(powerAllOff.arg1, 1);
	SPIDataTransmit(powerAllOff.arg2, 1);
	SPIDataTransmit(powerAllOff.end1, 1);
	SPIDataTransmit(powerAllOff.end2, 1);

//	sendPacket((unsigned char*)&powerAllOff, 1, (unsigned char*)&powerAllOffRecv);
}

void changeSwitch(unsigned int Switch, unsigned int state)
{
	CMDPacket changeSwitch;
	RECVPacket changeSwitchRecv;

	changeSwitch.start = 0xAA;
	changeSwitch.cmd = 0xD0;
	changeSwitch.arg1 = Switch & 0xFF;
	changeSwitch.arg2 = state & 0xFF; // 0 = turn, 1 = straight
	changeSwitch.end1 = 0x55;
	changeSwitch.end2 = 0xFF;

//	sendPacket((unsigned char*)&changeSwitch, 2, (unsigned char*)&changeSwitchRecv);
}

void resetHall()
{

}

void resetAllHalls()
{

}

