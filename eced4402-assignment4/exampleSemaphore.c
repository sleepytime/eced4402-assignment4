/*
 * exampleSemaphore.c
 *
 *  Created on: 2015-11-30
 *      Author: Andy
 */

#include "semaphore.h"

volatile int globalVar = 0;


unsigned int semaphore = SEMA_OPEN;

void proc1(){
	unsigned long count = 1000;
	while(count-->0){
		semaWait(&semaphore);
		globalVar++;
		semaPost(&semaphore);
	}
}
