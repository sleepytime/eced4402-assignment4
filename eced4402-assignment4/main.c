/*
 * main.c
 */

/*
 * Atmel 1 - Hall sensor signals (1-8), section control, hall sensor reset (all sensors)
 * Atmel 2 - Switch control (1-9)
 * Atmel 3 - Hall sensor signal (9-32)
 *
 *
 */

#include <stdio.h>
#include "gpioe.h"
#include "gpiof.h"
#include "packet.h"
#include "routing.h"
#include "semaphore.h"
#include "sensors.h"
#include "spi.h"
#include "sysctl.h"

#define NVIC_EN0_R		(*((volatile unsigned long *)0xE000E100))	// Interrupt 0-31 Set Enable Register
#define NVIC_EN1_R		(*((volatile unsigned long *)0xE000E104))	// Interrupt 32-54 Set Enable Register

void IntEnable(unsigned long InterruptIndex)
{
	// Depending on interrupt number you are either adjusting EN0 or EN1 register:
	// Enable the interrupt in the EN0 Register
	if(InterruptIndex < 32)
	{
		NVIC_EN0_R = 1 << InterruptIndex;
	}
	else
	{
		// Enable the interrupt in the EN1 Register
		NVIC_EN1_R = 1 << (InterruptIndex - 32);
	}
}

int main(void) {
	IntEnable(30); // Same function from previous assignments. Just copy from there. Arg 30 for Port F
	printf("1\n");
	IntEnable(4); // Enables Port E in NVIC table.
	printf("2\n");
	PORTF_Init();
	printf("3\n");
	init_ccw_sections();
	printf("4\n");
	init_cw_sections();
	printf("5\n");
	SPI_Init();
	printf("6\n");



//	printf("Finished initialization\n");
//	printf("Powering sections on.\n");
//	powerOn(1, CCW, 1);
//	powerOn(2, CCW, 1);
//	powerOn(3, CCW, 1);
//	powerOn(4, CCW, 1);
//	powerOn(5, CCW, 1);
//	powerOn(6, CCW, 1);
	powerAllOn(CCW, 3);
	printf("7\n");
	
//	printf("Press any button to quit.\n");
	getchar();

	powerAllOff(); // Should be renamed to powerOffAll().
	return 0;
}
