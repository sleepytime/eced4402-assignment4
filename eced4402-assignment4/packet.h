/*
 * packet.h
 *
 *  Created on: 2015-11-27
 *      Author: Andy
 */

#ifndef PACKET_H_
#define PACKET_H_

#define PACKET_SIZE	6

typedef struct
{
	unsigned char start;
	unsigned char cmd;
	unsigned char arg1;
	unsigned char arg2;
	unsigned char end1;
	unsigned char end2;
}CMDPacket;

typedef struct
{
	unsigned char start1;
	unsigned char start2;
	unsigned char resp1;
	unsigned char resp2;
	unsigned char resp3;
	unsigned char result;
}RECVPacket;

void sendPacket(unsigned char*, int, unsigned char*);
void powerOn(unsigned int, unsigned int, unsigned int);
void powerOff(unsigned int);
void powerAllOn(unsigned int, unsigned int);
void powerAllOff();
void changeSwitch(unsigned int, unsigned int);
void resetHall();
void resetAllHalls();


#endif /* PACKET_H_ */
