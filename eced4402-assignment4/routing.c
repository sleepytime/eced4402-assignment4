/*
 * routing.c
 *
 *  Created on: 2015-11-30
 *      Author: Andy
 */

// When to == from, destination is reached

//func(from)_(to)

#include "routing.h"

//void (*RoutingMatrix[TOTAL_SECTIONS][TOTAL_SECTIONS])() = {
//		{func1_1, func1_2, func1_3, func1_4, func1_5, func1_6, func1_7, func1_8, func1_9, func1_10, func1_11, func1_12, func1_13, func1_14, func1_15, func1_16},
//		{func2_1, func2_2, func2_3, func2_4, func2_5, func2_6, func2_7, func2_8, func2_9, func2_10, func2_11, func2_12, func2_13, func2_14, func2_15, func2_16},
//		{func3_1, func3_2, func3_3, func3_4, func3_5, func3_6, func3_7, func3_8, func3_9, func3_10, func3_11, func3_12, func3_13, func3_14, func3_15, func3_16},
//		{func4_1, func4_2, func4_3, func4_4, func4_5, func4_6, func4_7, func4_8, func4_9, func4_10, func4_11, func4_12, func4_13, func4_14, func4_15, func4_16},
//		{func5_1, func5_2, func5_3, func5_4, func5_5, func5_6, func5_7, func5_8, func5_9, func5_10, func5_11, func5_12, func5_13, func5_14, func5_15, func5_16},
//		{func6_1, func6_2, func6_3, func6_4, func6_5, func6_6, func6_7, func6_8, func6_9, func6_10, func6_11, func6_12, func6_13, func6_14, func6_15, func6_16},
//		{func7_1, func7_2, func7_3, func7_4, func7_5, func7_6, func7_7, func7_8, func7_9, func7_10, func7_11, func7_12, func7_13, func7_14, func7_15, func7_16},
//		{func8_1, func8_2, func8_3, func8_4, func8_5, func8_6, func8_7, func8_8, func8_9, func8_10, func8_11, func8_12, func8_13, func8_14, func8_15, func8_16},
//		{func9_1, func9_2, func9_3, func9_4, func9_5, func9_6, func9_7, func9_8, func9_9, func9_10, func9_11, func9_12, func9_13, func9_14, func9_15, func9_16},
//		{func10_1, func10_2, func10_3, func10_4, func10_5, func10_6, func10_7, func10_8, func10_9, func10_10, func10_11, func10_12, func10_13, func10_14, func10_15, func10_16},
//		{func11_1, func11_2, func11_3, func11_4, func12_5, func11_6, func11_7, func11_8, func11_9, func11_10, func11_11, func11_12, func11_13, func11_14, func11_15, func11_16},
//		{func12_1, func12_2, func12_3, func12_4, func12_5, func12_6, func12_7, func12_8, func12_9, func12_10, func12_11, func12_12, func12_13, func12_14, func12_15, func12_16},
//		{func13_1, func13_2, func13_3, func13_4, func13_5, func13_6, func13_7, func13_8, func13_9, func13_10, func13_11, func13_12, func13_13, func13_14, func13_15, func13_16},
//		{func14_1, func14_2, func14_3, func14_4, func14_5, func14_6, func14_7, func14_8, func14_9, func14_10, func14_11, func14_12, func14_13, func14_14, func14_15, func14_16},
//		{func15_1, func15_2, func15_3, func15_4, func15_5, func15_6, func15_7, func15_8, func15_9, func15_10, func15_11, func15_12, func15_13, func15_14, func15_15, func15_16},
//		{func16_1, func16_2, func16_3, func16_4, func16_5, func16_6, func16_7, func16_8, func16_9, func16_10, func16_11, func16_12, func16_13, func16_14, func16_15, func16_16}
//};
