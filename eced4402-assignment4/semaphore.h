/*
 * semaphore.h
 *
 *  Created on: 2015-11-30
 *      Author: Andy
 */

#ifndef SEMAPHORE_H_
#define SEMAPHORE_H_

#define SEMA_OPEN  	0
#define SEMA_CLOSED	1

void semaWait(unsigned int *semaphore);
void semaPost(unsigned int *semaphore);

#endif /* SEMAPHORE_H_ */
