/*
 * sensors.c
 *
 *  Created on: 2015-11-30
 *      Author: Andy
 */

#include "sensors.h"

void init_ccw_sections()
{
	// Define sections as increasing in the counterclockwise direction.

	int i;
	// Should call allocate function from assignment 1
	Section ccw_section[TOTAL_SECTIONS+1]; // Section 17 reserved for dead-ends

	for(i = 0; i<OUTER_SECTIONS; i++) // Initialize outer tracks
	{
		ccw_section[i].FirstHall = (i+1)*2;
		ccw_section[i].SecondHall = ((i+1)*2) - 1;
		ccw_section[i].NextSection = (i+1) % OUTER_SECTIONS;
		ccw_section[i].PrevSection = (i-1) % OUTER_SECTIONS;
	}

	for(i = OUTER_SECTIONS+1; i<INNER_SECTIONS; i++) // Initialize inner tracks
	{
		ccw_section[i].FirstHall = (i+1)*2;
		ccw_section[i].SecondHall = ((i+1)*2) - 1;
		ccw_section[i].NextSection = (i+1) % INNER_SECTIONS;
		ccw_section[i].PrevSection = (i-1) % INNER_SECTIONS;
	}

	// Special Cases
	ccw_section[12].FirstHall = (12+1)*2;
	ccw_section[12].SecondHall = ((12+1)*2) - 1;
	ccw_section[12].NextSection = 2;
	ccw_section[12].PrevSection = 1;

	ccw_section[13].FirstHall = (13+1)*2;
	ccw_section[13].SecondHall = ((13+1)*2) - 1;
	ccw_section[13].NextSection = TOTAL_SECTIONS;
	ccw_section[13].PrevSection = 1;

	// section[14] = 15 on diagram. Going CCW actually puts you on track instead of a deadend.
	ccw_section[14].FirstHall = (14+1)*2;
	ccw_section[14].SecondHall = ((14+1)*2) - 1;
	ccw_section[14].NextSection = 11;
	ccw_section[14].PrevSection = TOTAL_SECTIONS;

	ccw_section[15].FirstHall = (15+1)*2;
	ccw_section[15].SecondHall = ((15+1)*2) - 1;
	ccw_section[15].NextSection = TOTAL_SECTIONS;
	ccw_section[15].PrevSection = 3;
}

void init_cw_sections()
{
	// Define sections as increasing in the counterclockwise direction.

	int i;
	// Should call allocate function from assignment 1
	Section cw_section[TOTAL_SECTIONS+1]; // Section 17 reserved for dead-ends

	for(i = 0; i<OUTER_SECTIONS; i++) // Initialize outer tracks
	{
		cw_section[i].SecondHall = (i+1)*2;
		cw_section[i].FirstHall = ((i+1)*2) - 1;
		cw_section[i].PrevSection = (i+1) % OUTER_SECTIONS;
		cw_section[i].NextSection = (i-1) % OUTER_SECTIONS;
	}

	for(i = OUTER_SECTIONS+1; i<INNER_SECTIONS; i++) // Initialize inner tracks
	{
		cw_section[i].SecondHall = (i+1)*2;
		cw_section[i].FirstHall = ((i+1)*2) - 1;
		cw_section[i].PrevSection = (i+1) % INNER_SECTIONS;
		cw_section[i].NextSection = (i-1) % INNER_SECTIONS;
	}

	// Special Cases
	cw_section[12].SecondHall = (12+1)*2;
	cw_section[12].FirstHall = ((12+1)*2) - 1;
	cw_section[12].PrevSection = 2;
	cw_section[12].NextSection = 1;

	cw_section[13].SecondHall = (13+1)*2;
	cw_section[13].FirstHall = ((13+1)*2) - 1;
	cw_section[13].PrevSection = TOTAL_SECTIONS;
	cw_section[13].NextSection = 1;

	// section[14] = 15 on diagram. Going CCW actually puts you on track instead of a deadend.
	cw_section[14].SecondHall = (14+1)*2;
	cw_section[14].FirstHall = ((14+1)*2) - 1;
	cw_section[14].PrevSection = 11;
	cw_section[14].NextSection = TOTAL_SECTIONS;

	cw_section[15].SecondHall = (15+1)*2;
	cw_section[15].FirstHall = ((15+1)*2) - 1;
	cw_section[15].PrevSection = TOTAL_SECTIONS;
	cw_section[15].NextSection = 3;
}
