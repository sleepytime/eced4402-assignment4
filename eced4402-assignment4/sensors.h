/*
 * sensors.h
 *
 *  Created on: 2015-11-30
 *      Author: Andy
 */

#ifndef SENSORS_H_
#define SENSORS_H_

#define OUTER_SECTIONS	6
#define INNER_SECTIONS	12
#define TOTAL_SECTIONS	16

// Switch controls
#define STRAIGHT	1
#define TURN		0

typedef struct
{
	int FirstHall;
	int SecondHall;
	int NextSection;
	int PrevSection;
}Section;

void init_ccw_sections(void);
void init_cw_sections(void);

#endif /* SENSORS_H_ */
